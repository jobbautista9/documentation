# Devuan Style Guide

This is an ongoing process to define and refine all communications and design elements for Devuan projects, from user interfaces to print.  We aim to provide reusable elements and usage guidelines to improve brand recognition and facilitate prototyping new projects.

## Introduction

Why, audiences, principles, usage, contribution, collaborative design.

## Visual Design

This is a collection of assets and guidelines to provide the _Devuan touch_.

### [Logotype](logotype)
### [Colors](colors)
### [Typography](typography)
### [Iconography](iconography)

## Interaction Design

This is a collection of patterns for the Web, some are useful for desktop applications as well.

### Scaffolding
### Forms
### Tables
### Media
### JavaScript
