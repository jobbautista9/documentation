Dieses Dokument zeigt, wie man von Devuan Jessie zu Devuan ASCII aufrüsten kann. Es wird davon ausgegangen, dass ein funktionierendes Devuan Jessie-System bereits installiert ist. Es sollte nicht für Migrationen verwendet werden.

# Upgrade von Devuan Jessie zu ASCII
Bearbeite zuerst die Datei sources.list, damit der Zweig in ASCII geändert werden kann.

`root@devuan:~# editor /etc/apt/sources.list`

Füge den aktuellen Devuan-Spiegel und den ASCII-Zweig wie gezeigt hinzu. Kommentiere alle anderen Zeilen aus.

~~~
deb http://deb.devuan.org/merged ascii main
deb http://deb.devuan.org/merged ascii-updates main
deb http://deb.devuan.org/merged ascii-security main
deb http://deb.devuan.org/merged ascii-backports main
~~~

Aktualisiere den Devuan Schlüsselbund, um sicherzustellen, dass Du die neueste Version hast.

`root@devuan:~# apt-get upgrade devuan-keyring`

Aktualisiere die Paketindexdateien.

`root@devuan:~# apt-get update`

Das einzige, was noch zu tun ist, ist ein Upgrade des Systems.

`root@devuan:~# apt-get dist-upgrade`

---

<sub>**Diese Arbeit wurde unter der Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de)] Lizenz veröffentlicht. Alle Marken sind Eigentum ihrer jeweiligen Inhaber. Diese Arbeit wird "WIE BESEHEN" zur Verfügung gestellt und kommt mit KEINERLEI Garantie.**</sub>
