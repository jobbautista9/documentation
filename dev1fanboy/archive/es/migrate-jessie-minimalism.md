# Migración a Devuan Jessie con minimalismo
Esta guía muestra todos los pasos necesarios para migrar a Devuan, configurar el gestor de paquetes apt para una instalación mínima, eliminar D-Bus del sistema y gestionar la red.

## Migración a Devuan
Para iniciar la migración comenzar editando el archivo sources.list a fin de actualizar los índices de paquetes con apt-get.

`root@debian:~# editor /etc/apt/sources.list`

Comentar todas las líneas en el archivo sources.list y agregar las siguientes.

~~~
deb http://pkgmaster.devuan.org/merged jessie main
deb http://pkgmaster.devuan.org/merged jessie-updates main
deb http://pkgmaster.devuan.org/merged jessie-security main
deb http://pkgmaster.devuan.org/merged jessie-backports main
~~~

Actualizar los índices de paquetes para poder descargar paquetes desde los repositorios de Devuan.

`root@debian:~# apt-get update`

Antes de poder instalar nuevos paquetes es necesario instalar el *keyring* de Devuan y actualizar los índices nuevamente para poder autenticar los paquetes.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Con el *keyring* de Devuan instalado se deben actualizar los índices nuevamente para que los paquetes sean autenticados a partir de ahora.

`root@debian:~# apt-get update`

Finalizar el proceso de actualización.

`root@debian:~# apt-get dist-upgrade`

## Configuración minimalista
Gracias a un tip de [TheFlash] es posible hacer una limpieza del sistema de forma elegante. El truco consiste en configurar apt para marcar los paquetes recomendados como no importantes.

`root@devuan:~# editor /etc/apt/apt.conf.d/01lean`

Agregar las siguientes líneas:

~~~
APT::Install-Recommends "0";
APT::AutoRemove::RecommendsImportant "false";
~~~

Mientras que la mayoría de los paquetes recomendados no son importantes por naturaleza, existen algunos paquetes que se deben proteger para que no sean removidos.

Por la seguridad de los navegadores y otras aplicaciones que hacen uso de OpenSSL, se debe asegurar que los certificados SSL de las autoridades certificantes de confianza estén siempre disponibles. Para ello se instala el paquete ca-certificates. Evitar este paso sólo si se tiene certeza sobre lo que se está haciendo.

`root@devuan:~# apt-get install ca-certificates`

El paquete que contiene los certificados SSL es ahora marcado como instalado manualmente en lugar de ser una dependencia o recomendación. Si el siguiente paso muestra paquetes que se deseen mantener es posible hacer lo mismo antes de confirmar su eliminación.

`root@devuan:~# apt-get autoremove --purge`

## Devuan sin D-Bus
Remover D-Bus es un tanto más complejo y requiere cierto compromiso.

### Montar sistemas de archivo en espacio usuario
Una alternativa a los sistemas de auto-montaje dependientes de D-Bus consiste en definir los puntos de montaje de forma manual, e instalar un gestor de archivos que pueda montar volúmenes sin depender de D-Bus.

Ya que se editará el archivo fstab, se debería hacer una copia de respaldo primero.

`root@devuan:~# cp /etc/fstab /etc/fstab.backup`

Editar el archivo fstab.

`root@devuan:~# editor /etc/fstab`

Agregar las siguientes líneas, sustituyendo el nombre de dispositivo correcto en caso de que sea diferente. Asegurarse de establecer la opción `user` para permitir que los usuarios no privilegiados puedan montar el dispositivo.

~~~
/dev/sdb1        /media/usb0    auto    user,noauto    0 0
~~~

Ahora es posible conectar el dispositivo USB y recurrir al utilitario `lsblk` para determinar el nodo de dispositivo correcto.

Crear el punto de montaje donde los dispositivos USB serán montados.

`root@devuan:~# mkdir /media/usb0`

Conectar un dispositivo USB y verificar el funcionamiento como un usuario regular.

`user@devuan:~$ mount -v /media/usb0`

`user@devuan:~$ umount -v /media/usb0`

### Software independiente de D-Bus
La mayoría de los entornos de escritorio requieren D-Bus, por lo tanto debe utilizarse un gestor de ventanas en su lugar. En este documento se recomienda fluxbox ya que es intuitivo, liviano y puede ser instalado fácilmente.

`root@devuan:~# apt-get install fluxbox menu fbpager feh`

Ejecutar update-alternatives para que startfluxbox sea el gestor de ventanas por defecto.

`root@devuan:~# update-alternatives --config x-window-manager`

También se puede incluir WDM.

`root@devuan:~# apt-get install wdm`

De otra forma simplemente se debe invocar al script startx desde consola luego del login en la cuenta de usuario.

`user@devuan:~$ startx`

Para contar con un gestor de archivos con capacidad de montar dispositivos removibles sin necesidad de un mecanismo de auto-montaje, se puede recurrir a xfe.

`root@devuan:~# apt-get install xfe`

Una buena elección para un navegador Web es firefox-esr, ya que no depende directamente de D-Bus.

`root@devuan:~# apt-get install firefox-esr`

### Configurar la red
En lugar de utilizar un gestor de redes dependiente de D-Bus se configura la red para utilizar múltiples interfaces de red de forma manual.

`root@devuan:~# editor /etc/network/interfaces`

La siguiente configuración muestra un ejemplo para múltiples redes inalámbricas en la misma interfaz. Al agregar una sección para una red que se usa sólo de vez en cuando, es posible alterar la configuración por defecto cuando sea necesario. Para más información ver la [Debian Reference](https://www.debian.org/doc/manuals/debian-reference/ch05.en.html#_the_manually_switchable_network_configuration) sobre *switchable network configuration* para redes inalámbricas.

~~~
allow-hotplug wlan0
iface wlan0 inet dhcp
        wpa-ssid myssid
        wpa-psk mypassphrase

iface work inet dhcp
        wpa-ssid myssid
        wpa-psk mypassphrase
~~~

Por ejemplo es posible cambiar a la red "work" ejecutando `ifdown wlan0` y luego `ifup wlan0=work` como root.

La configuración de red cableada es mucho más simple.

~~~
# Configuración de red automática, activa sólo cuando se detecta link.
allow-hotplug eth0
iface eth0 inet dhcp

# Configuración de red estática, se activa siempre al iniciar el sistema.
auto eth1
iface eth1 inet static
        address 192.168.1.5
        netmask 255.255.255.0
        gateway 192.168.1.1
~~~

Para más información ver `man 5 interfaces`.

## Últimos pasos
Se requiere un reinicio por única vez para eliminar a systemd como PID 1.

`root@devuan~# reboot`

Ahora es posible eliminar systemd y dbus de manera segura.

`root@devuan:~# apt-get purge systemd systemd-shim libsystemd0 dbus`

El escritorio gnome actualmente no es usable sin systemd. Utilizar una expresión regular para eliminar todos los paquetes pertenecientes a gnome.

`root@devuan~# apt-get purge .*gnome.*`

Puede que sea necesario proteger el paquete xorg para evitar que sea removido del sistema.

`root@devuan~# apt-get install xorg`

Tal vez se desee eliminar los recomendados y huérfanos nuevamente.

`root@devuan:~# apt-get autoremove --purge`

Al igual que eliminar archivos de paquetes obsoletos dejados por la instalación de Debian.

`root@devuan:~# apt-get autoclean`

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

