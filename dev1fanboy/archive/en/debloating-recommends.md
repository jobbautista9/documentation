# Debloating recommends
This document describes how to configure APT to treat recommends as optional and retroactively remove them. This removes optional packages that are not wanted or used in many cases. Thanks go to [TheFlash] for pointing out this feature of APT.

## Configuring APT
We will first configure APT to treat recommended packages as unimportant.

`root@devuan:~# editor /etc/apt/apt.conf.d/01lean`

Configure APT so that recommended packages are treated as optional.

~~~
APT::Install-Recommends "0";
APT::AutoRemove::RecommendsImportant "false";
~~~

## Protecting important packages
Whilst most recommended packages are not of an important nature, there are some packages you should protect from possible removal.

For the security of your browsers and other applications, we should make sure that SSL certificates will always be available by installing the ca-certificates package. Only skip this step if you know what you're doing.

`root@devuan:~# apt-get install ca-certificates`

The SSL certificates package will now be marked as a manually installed package, instead of a dependency or recommended package. If the next step shows packages you wish to keep you can do the same for them before confirming any removals.

## Cleaning out unwanted packages
You can now retroactively remove all recommended packages that have become orphaned by the configuration changes.

`root@devuan:~# apt-get autoremove --purge`

Now that we've removed all these packages, we no longer need them archived in the cache.

`root@devuan:~# apt-get autoclean`

---

<sub>**This work is released under the Creative Commons Attribution-ShareAlike 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)] license. All trademarks are the property of their respective owners. This work is provided "AS IS" and comes with absolutely NO warranty.**</sub>
