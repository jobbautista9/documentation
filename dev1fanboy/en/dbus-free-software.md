# D-Bus free software in Devuan
This lists software included in Devuan that is completely free of D-Bus uncumberment.

## Window managers
With suggestions from the Devuan community.

* fluxbox
* blackbox
* openbox
* fvwm 
* fvwm-crystal
* icewm
* metacity
* evilwm
* windowmaker
* mutter
* sawfish
* icewm
* dwm
* i3
* wmaker

## File managers
* xfe
* pcmanfm
* rox-filer
* mc
* gentoo
* fdclone
* s3dfm
* tkdesk
* ytree
* vifm

## Display managers
* nodm
* ldm
* xdm
* wdm

## Web browsers
* midori
* surf
* dillo
* links2
* links
* elinks
* lynx
* edbrowse
* w3m

## Music players
* deadbeef
* cmus
* cdcd
* lxmusic
* randomplay

## Media players
* totem
* xine-ui
* mplayer2
* mpv
* melt

## IRC clients
* weechat
* irssi
* ekg2-ui-gtk
* scrollz
* loqui

---

<sub>**This work is released under the Creative Commons Attribution-ShareAlike 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)] license. All trademarks are the property of their respective owners. This work is provided "AS IS" and comes with absolutely NO warranty.**</sub>
