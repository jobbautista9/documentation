This document shows how to upgrade from Devuan Jessie to Devuan ASCII. It assumes a working Devuan Jessie system is already installed and should not be used for migrations.

# Upgrade from Devuan Jessie to ASCII
First edit the sources.list file so that the branch can be changed to ASCII.

`root@devuan:~# editor /etc/apt/sources.list`

Add the latest Devuan mirror and the ASCII branch as shown. Comment out any other lines.

~~~
deb http://deb.devuan.org/merged ascii main
deb http://deb.devuan.org/merged ascii-updates main
deb http://deb.devuan.org/merged ascii-security main
deb http://deb.devuan.org/merged ascii-backports main
~~~

Upgrade the Devuan keyring to make sure you have the latest version.

`root@devuan:~# apt-get upgrade devuan-keyring`

Update the package index files.

`root@devuan:~# apt-get update`

The only thing left to do is upgrade the system.

`root@devuan:~# apt-get dist-upgrade`

---

<sub>**This work is released under the Creative Commons Attribution-ShareAlike 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)] license. All trademarks are the property of their respective owners. This work is provided "AS IS" and comes with absolutely NO warranty.**</sub>
