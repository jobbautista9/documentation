This document provides generic instructions for configuring the network when migrating from Debian to Devuan.

# Network configuration

When migrating you can choose between using wicd or configuring the network manually.

Manual configuration has the advantage of not requiring dbus or a network manager. For remote migrations you should always use manual network configuration or you will very likely lose network access during migration.

Using a network manager has the advantage of being easy to use and having a GUI interface. This method should be used **only** for migration instructions that did not include wicd installation.

## Using the wicd network manager

You first need to install the wicd package. The full package is used here to provide the gtk interface and also the curses interface for the console.

`root@debian:~# apt-get install wicd`

The network manager daemon should be stopped and disabled at boot time.

`root@debian:~# service network-manager stop`  
`root@debian:~# update-rc.d -f network-manager remove`

Restart the wicd daemon to make it active in managing the network.

`root@debian:~# service wicd restart`

Wireless users should now configure their network from the wicd gtk client within their desktop environment.

## Manual wired network configuration

In newer Debian releases you will be presented with the counter intuitive interface names, such as `ens3` so for safety add lines for your current interface name as well as the intuitive device name.

Note that after migration the Debian style interface will not be found so will simply not be configured.

Wired connections can be managed by adding configuration lines to the `interfaces` file.

`root@debian:~# editor /etc/network/interfaces`

### Automatic network configuration
This will configure the network automatically whenever a link is detected. Adjust this according to your interface name if necessary.

~~~
allow-hotplug eth0
iface eth0 inet dhcp

allow-hotplug ens3
iface ens3 inet dhcp
~~~

### Static network configuration

Or configure the network statically.

~~~
auto eth0
iface eth0 inet static
        address 192.168.1.2
        netmask 255.255.255.0
        gateway 192.168.1.1

auto ens3
iface ens3 inet static
        address 192.168.1.2
        netmask 255.255.255.0
        gateway 192.168.1.1
~~~

## Manual wireless network configuration
This is similar to wired networks except you will need to provided authentication details. If you need automatic network configuration, you should use the wicd network manager.

Edit the interfaces file to configure the host on the wireless network.

`root@debian:~# editor /etc/network/interfaces`

Adjust the interfaces file to suit your interface and network configuration.

~~~
allow-hotplug wlan0
iface wlan0 inet dhcp
        wpa-ssid myssid
        wpa-psk mypassphrase
~~~

---

<sub>**This work is released under the Creative Commons Attribution-ShareAlike 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)] license. All trademarks are the property of their respective owners. This work is provided "AS IS" and comes with absolutely NO warranty.**</sub>
