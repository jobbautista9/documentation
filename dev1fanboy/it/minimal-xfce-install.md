# Installazione minimale di XFCE 
Questo documento descrive come eseguire un'installazione minimale di xfce4 insieme a qualche extra per un ambiente desktop più completo.

## Installazione di XFCE
Installate i pacchetti necessari e sufficienti a far partire il nuovo ambiente desktop.

`root@devuan:~# apt-get install xfce4-panel xfdesktop4 xfwm4 xfce4-settings xfce4-session`

Potreste voler installare anche questi pacchetti per un desktop più accessoriato.

`root@devuan:~# apt-get install xfce4-terminal xfce4-appfinder thunar xfce4-power-manager ristretto cinnabar-icon-theme`

### Aggiungere il supporto per l'auto-mounting
Installate i pacchetti necessari a thunar (il gestore file di XFCE) per supportare l'auto-mounting.

`root@devuan:~# apt-get install thunar-volman gvfs policykit-1`

Dopo aver avviato l'ambiente desktop potete usare il gestore delle preferenze di XFCE per configurare l'auto-mounting.

### Aggiungere un gestore grafico di login
Qualora sia richiesto un gestore grafico del login potete installare slim, che costituisce la scelta di default di Devuan.

`root@devuan:~# apt-get install slim`

Ora potete impostare xfce4-session come x-session-manager servendovi di update-alternatives. 

`root@devuan:~# update-alternatives --config x-session-manager`

### Usare XFCE senza un gestore grafico di login
Al terminale effettuate il login come utente non privilegiato e usate lo script di avvio startxfce4.

`user@devuan:~$ startxfce4`

---

<sub>Traduzione di antoniotrkdz<sub>

<sub>**Questa guida è rilasciata in base alla licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it)]. Tutti i marchi sono proprietà dei rispettivi depositari. Le informazioni in questa guida vengono fornite "COME SONO" e non sono coperte da alcuna garanzia.**</sub>
