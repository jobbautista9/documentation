# Guia de instalação do Devuan

Este é um guia fácil de acompanhar para instalar o Devuan de uma imagem CD/DVD em hardware suportado. A recomendação geral ao acompanhar este guia é de que sempre faça backup de seus dados antes de prosseguir.

## Conteúdo

[Pré-requisitos](#pré-requisitos)  
[Arquiteturas suportadas](#arquiteturas-suportadas)  
[Imagens de instalação](#imagens-de-instalação)  
[Instalando Devuan](#instalando-devuan)

## Pré-requisitos

Você precisa saber pelo menos como escrever uma imagem ISO a um CD/DVD ou dispositivo USB, e fazer seu computador inicializar a partir dele. Para aqueles que já usam GNU/Linux ou similares, podemos cobrir as etapas para isso.

## Arquiteturas suportadas

* amd64
* i386

## Imagens de instalação

Essas são atualmente as maneiras de conseguir imagens de instalação.

* Diretamente do [Arquivo de lançamento do Devuan](https://files.devuan.org)
* Dos espelhos listados em [devuan.org](https://www.devuan.org) que podem ser mais próximos a você
* Via [torrent](https://files.devuan.org/devuan_beowulf.torrent) para os lançamentos estáveis

Se você tem acesso à Internet, é recomendado que use uma imagem de um único DVD completo para a instalação; caso contrário, você irá preferir a coleção completa.

Por favor use espelhos ou torrents se possível.

Aqueles que não utilizam a linha de comando podem [pular para a instalação](#installing-devuan).

### Verifique a integridade das imagens

Antes de escrever uma imagem ao seu dispositivo removível, é melhor verificar a integridade para que você possa ter certeza de que a imagem está em bom estado. Isso evita muitos problemas que podem ocorrer durante a instalação.

Baixe o `SHA256SUMS` do [arquivo de lançamento](https://files.devuan.org) e verifique a integridade da imagem.

`user@hostname:~$ sha256sum --ignore-missing -c SHA256SUMS`

### Verifique as imagens

Imagens de instalação distribuídas pelo Devuan são assinadas de modo que possam ser verificadas com origem do Devuan. Verificar imagens permite que saiba se foram alteradas antes de recebê-las.

Obtenha as [chaves de assinatura](https://files.devuan.org/devuan-devs.gpg) dos desenvolvedores Devuan e importe-as para seu chaveiro.

`user@hostname:~$ gpg --import devuan-devs.gpg`

Use o `SHA256SUMS.asc` assinado do arquivo de lançamento para verificar a imagem.

`user@hostname:~$ gpg --verify SHA256SUMS.asc`

Uma "boa assinatura" indica que está tudo bem.

### Escrevendo uma imagem a um CD/DVD ou dispositivo USB.

Imagens podem ser escritas a um CD ou DVD usando wodim.

`user@hostname:~$ wodim dev=/dev/sr0 -eject filename.iso`

Todas as imagens ISO do Devuan são híbridas, e podem ser escritas a um dispositivo USB usando o dd.

`root@hostname:~# dd if=filename.iso of=/dev/sdX bs=1M && sync`

## Instalando Devuan

Para a instalação usaremos o instalador não gráfico. Esse é geralmente mais rápido que um instalador gráfico e requer menos recursos. Isso deve ser apropriado para instalar em uma máquina virtual, assim como em hardware físico. Embora isso seja intimidador para iniciantes, não há nada a temer pois você será guiado pelo processo.

&nbsp;

**1) Inicie o sistema a partir do CD/DVD ou dispositivo USB e selecione a opção `Install`. O restante das opções estão fora do alcance deste guia.**

![Instalação do Devuan, primeira inicialização](../img/firstboot.png)

&nbsp;

**2) As próximas etapas irão perguntar sobre sua linguagem, localização e disposição de teclado.**

![Disposição de teclado](../img/keyboard.png)

&nbsp;

**3) O instalador irá configurar sua rede automaticamente, mas usuários de rede sem fio terão que fornecer o SSID (nome do ponto de acesso) e a senha. Será então requisitado um nome de host para o novo sistema. Não é possível usar espaços ou caracteres especiais.**

![Nome de host](../img/hostname.png)

&nbsp;

**4) Será requisitado um nome de domínio. Se você não precisa disso ou não sabe o que é e para que serve, deixe em branco.**

![Nome de domínio](../img/domain.png)

&nbsp;

**5) É recomendado que configure uma senha root para o Devuan. É uma boa prática de segurança usar uma senha forte. Você terá que digitar a senha novamente para garantir que a digitou corretamente.**

![Senha root](../img/rootpw.png)

&nbsp;

**6) Você terá que configurar uma conta de usuário, que é recomendado na maioria dos casos. Deixe o nome completo em branco a menos que tenha razões para preenchê-lo, e prossiga para definir o nome de usuário.**

![Nome de usuário](../img/username.png)

&nbsp;

**7) Defina uma senha para esse usuário e repita conforme pedido para garantir que digitou corretamente.**

![Senha de usuário](../img/userpw.png)

&nbsp;

**8) O instalador irá configurar o relógio usando [NTP](https://en.wikipedia.org/wiki/Network_Time_Protocol). Forneça seu fuso horário e continue com a instalação.**

![Configure o relógio](../img/clock.png)

&nbsp;

**9) Antes de instalar o Devuan o disco precisa ser particionado. Se a opção de usar o maior espaço contínuo estiver disponível, é recomendado que o faça. Isso irá preservar partições existentes sem tocá-las. Caso não tenha nada a preservar nele, use todo o disco.**

**Se você precisa de criptografia completa do disco, veja [aqui](full-disk-encryption.md) antes de continuar.**

![Use o maior espaço em disco](../img/partdisks1.png)

&nbsp;

**10) Escolher todos os arquivos em uma partição é uma opção razoável para iniciantes. Particionamento manual é além do escopo deste guia.**

![Todos os arquivos em uma partição](../img/partdisks2.png)

&nbsp;

**11) É hora de escrever as partições ao disco e formatá-las com sistemas de arquivo. Se você estiver satisfeito com as mudanças, escolha escrever ao disco e continue. Será necessário confirmar isso antes que mudanças sejam feitas.**

![Escrever mudanças ao disco](../img/partdisks3.png)

&nbsp;

**12) O sistema base será instalado. Dependendo do seu hardware isso pode levar algum tempo.**

![Aguardando o sistema base ser instalado](../img/installing.png)

&nbsp;

**13) O instalador irá perguntar se você deseja usar um espelho de rede. Se você tem acesso à Internet isso é recomendado, pois lhe dará acesso às versões mais recentes dos pacotes. Se escolher usar um espelho de rede, selecione um país da lista.**

![Use um espelho de rede](../img/mirror1.png)

&nbsp;

**14) Qualquer um dos espelhos fornecidos servirá. Se houver um espelho mais próximo de você ele será redirecionado automaticamente quando instalar pacotes.**

![Use um espelho de rede](../img/mirror2.png)

&nbsp;

**15) Devuan pode usar um concurso de popularidade (popcon) para coletar informações sobre os pacotes mais utilizados. Isso só é ativado sob a aprovação do usuário e irá coletar somente dados estatísticos sobre pacotes que são instalados a partir desse ponto.**

![Concurso de popularidade](../img/popcon.png)

&nbsp;

**16) A instalação fornece pacotes pré selecionados que você pode optar por instalar. Geralmente os padrões são uma boa opção e você deve permanecer com eles.**

![Seleção de software](../img/tasksel.png)

&nbsp;

**17) O instalador irá agora instalar os pacotes que você selecionou. Isso pode levar algum tempo.**

![Instalando software](../img/retrieving.png)

&nbsp;

**18) Agora que todo o software que você precisa está instalado, o gerenciador de inicialização GRUB será instalado. Ele permite a inicialização do sistema operacional após a instalação. Se for pedido para instalar no MBR, na maioria das vezes é isso que deve fazer.**

**Alguns sistemas não precisarão de mais nenhuma configuração e o instalador irá finalizar agora.**

![Instalando o inicializador GRUB](../img/grub-mbr.png)

&nbsp;

**19) É importante selecionar a localização correta do inicializador. Ele não deve ser instalado a uma partição, mas para a área MBR que se encontra no disco rígido.**

**Neste caso `/dev/sda` é o único disco rígido presente e portanto instalaremos nele.**

![Escolhendo onde instalar o gerenciador de inicialização](../img/grubtohdd.png)

&nbsp;

**20) A instalação finalizou, remova a mídia de instalação antes de prosseguir com a inicialização para o ambiente gráfico do Devuan.**

![Finalizando a instalação](../img/finish.png)


---
Traduzido por Douglas H. Silva

<sub>**Esta obra está licenciada com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)]. Todas as marcas são propriedades de seus respectivos donos. Este trabalho é fornecido "como é" e vem com absolutamente NENHUMA garantia.**</sub>
