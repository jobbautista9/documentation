# Instalação do Devuan GNU/Linux, atualizações e minimalismo
Esta documentação fornece informações sobre a instalação, atualização e migração para o Devuan, além de como reduzir o sistema a um estado mais minimalista. O wiki também está disponível em outras linguagens.

## Todos os lançamentos
[Informação geral](general-information.md)  
[Instalando Devuan](devuan-install.md)  
[Criptografia total de disco](full-disk-encryption.md)  
[Configuração de rede](network-configuration.md)  
[Devuan sem D-Bus](devuan-without-dbus.md)  
[Software independente do D-Bus](dbus-free-software.md)  
[Instalação Xorg minimalista](minimal-xorg-install.md)  
[Instalação com Xfce minimalista](minimal-xfce-install.md)

## ASCII
[Migrando para o ASCII](migrate-to-ascii.md)  
[Atualizando para o ASCII](upgrade-to-ascii.md)

## Traduções

Traduções foram disponibilizadas graças à equipe do wiki.

[Read in English](../) **|** [Auf Deutsch lesen](../de) **|** [Διαβάστε στα Ελληνικά](../el) **|** [Leer en Español](../es) **|** [Leggi in Italiano](../it)

---
Traduzido por Douglas H. Silva

<sub>**Esta obra está licenciada com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)]. Todas as marcas são propriedades de seus respectivos donos. Este trabalho é fornecido "COMO É" e vem com absolutamente NENHUMA garantia.**</sub>
