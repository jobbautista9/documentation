# Instalación mínima de XFCE
Este documento explica cómo realizar una instalación mínima de xfce4 junto con algunos extras opcionales que son generalmente necesarios.

## Instalar XFCE
Instalar los paquetes base suficientes para poder iniciar el entorno de escritorio.

`root@devuan:~# apt-get install xfce4-panel xfdesktop4 xfwm4 xfce4-settings xfce4-session`

Para contar con un escritorio más completo es posible instalar los siguientes paquetes adicionales.

`root@devuan:~# apt-get install xfce4-terminal xfce4-appfinder xfce4-power-manager thunar ristretto cinnabar-icon-theme`

### Agregar soporte para auto-montaje

Instalar los paquetes necesarios para que thunar (el gestor de archivos de xfce) pueda auto-montar sistemas de archivos.

`root@devuan:~# apt-get install thunar-volman gvfs policykit-1`

Luego de iniciar el entorno de escritorio es posible utilizar el gestor de configuraciones de xfce para habilitar el auto-montaje.

### Agregar un gestor de pantalla

En caso de necesitar un gestor de pantalla se recomienda slim, el cual se incluye por defecto en Devuan.

`root@devuan:~# apt-get install slim`

Ejecutar update-alternatives para establecer x-session-manager en xfce4-session.

`root@devuan:~# update-alternatives --config x-session-manager`

### Utilizar xfce sin un gestor de pantalla

Ingresar con una cuenta de usuario regular y ejecutar el script startxfce4 desde consola.

`user@devuan:~$ startxfce4`

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

