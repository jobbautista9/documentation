# Software no dependiente de D-Bus en Devuan
Este documento lista todo el software incluido en Devuan que es completamente libre de D-Bus.

## Gestores de ventanas
Con algunas sugerencias de la comunidad Devuan.

* fluxbox
* blackbox
* openbox
* fvwm 
* fvwm-crystal
* icewm
* metacity
* evilwm
* windowmaker
* mutter
* sawfish
* icewm
* dwm
* i3
* wmaker

## Gestores de archivos
* xfe
* pcmanfm
* rox-filer
* mc
* gentoo
* fdclone
* s3dfm
* tkdesk
* ytree
* vifm

## Gestores de pantalla
* nodm
* ldm
* xdm
* wdm

## Navegadores
* midori
* surf
* dillo
* links2
* links
* elinks
* lynx
* edbrowse
* w3m

## Reproductores de audio
* deadbeef
* cmus
* cdcd
* lxmusic
* randomplay

## Reproductores multimedia
* totem
* xine-ui
* mplayer2
* mpv
* melt

## Clientes IRC
* weechat
* irssi
* ekg2-ui-gtk
* scrollz
* loqui

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

