# [Devuan Weekly News][current] Issue XI

__Volume 02, Week 6, Devuan Week 11__

https://git.devuan.org/Envite/devuan-weekly-news/wikis/past-issues/volume-02/issue-011

## Editorial

From Devuan Weekly News we want to mourn the end of CrunchBang Linux.

Talking about something else, it seems that the list is becoming
two-fold. On one hand, it becomes concentrated on development, while
at the same time it discusses more philosophical issues. Maybe is it
the moment to separate into a dev list and a users list?


## Last Week in Devuan

### [libsysdev v0.1.0][1]

Isaac Dunham reported a file descriptor leak in `sysdev`. His patch
allows now to use `evdev` without `udev`.

### [Calamares][2]

Javier Ortega Conde suggested Devuan could use a
distribution-independent installer called __Calamares__.

### [Bastille-linux][3]

Unsteady Contemplation reports that Bastille-linux doesn't work with
debian anymore, and makes some comments on Debian's security. He also
shows us [some videos][4] while hating systemd.

We at DWN want to remind that we are moved by ___[datalove][5]___,
not hate. <3

### [Device Management][6]

In the long thread about device management, Ben remembers us that
device management is necessary, and explains why udev way is way
better that static allocation or the old Unix way.

### [Towards Systemd-Free Packages][7]

Jude Nelson asks whether there's __a list of packages dependent on
systemd__.  The quick answer is issue #6 of `packages-base`:

[Packages to be recompiled for standard desktop install][8]

This thread additionally offers a handful of sneak peeks at what's
cooking in Devuan behind the scenes:

- jaromil is working on `devuan-sdk`, a shell tool programmed in
  Zshell to provide a streamlined way of porting Debian packages to
  Devuan, and some other automation goodies ("installer and livecd
  baking").

- people willing to "__adopt a package__" can do so by submitting an
  issue to the [packages-base issue tracker][9].

- `i386` and `amd64` will probably be released before `arm` and other
  architectures.

- the VUA team settled on `Jenkins` for continuous integration instead
  of Debian's own dbuild.  Along with `Gitlab`, that announces quite a
  modern infrastructure.

### [Downgrades-towards systemd-free][10]

In a spinoff of the previously mentioned thread, Godefridus Daalmans
wonders that Devuan Jessie would need to keep some packages, like
cups, with a version lower than that of Debian Jessie, due to
newly introduced dependencies on systemd. He also proposes a wiki to
inform people on the downgrade process.

Jaromil answered that some packages [uploaded to jenkins][11] would be
better than the wiki.

Steve Litt proposes [lobbying upstreams to not depend on systemd][12].

Nikolaus Klepp suggested substituting the Desktop Environments like
Gnome or the current KDE for [Trinity Desktop Environment][13].

Adam Borowski tells us that some dependencies on systemd come from the
[Utopia stack][14], that might be used by MATE.

Neo Futur [cross-posts][15] to trinity-users to make them know Devuan
might be interested in TDE.

### [Enlightenment][16]

Robert Storey points us to the fact that Enlightenment in Debian is at
version 17 while version 19 is available. John Holland remembers that
Debian Jessie will have version 18.

### [X-GUI][17]

wiliam moss tells us that XFCE runs properly in FreeBSD, so it can not
have dependencies on systemd.

### [gufw][18]

macondo reports that gufw failed to install on Debian without systemd.
Nate Bargmann points that the dependency comes from the graphical
part through policykit since ufw does not have any such dependency.

Gravis suggests installing old versions of packages to avoid the newly
introduced dependencies. Adam Borowski suggests instead to

> recompile policykit with --disable-systemd -- it uses
ConsoleKit then.

### [KDE without systemd][19]

Ed Ender reports that Alien BOB has built KDE 5.2 without systemd on
Slackware.

### [CrunchBang Linux][20]

Nate Bargmann notices the end of the development of CrunchBang Linux
and wonders if systemd is one of the reasons. Several posters confirm
this thought.

Jaromil points out that we should welcome their community.

### [Package versions][21]

In the Good Ol' thread about how to version packages, tilt and Jaromil
confirm that +devuanX works for the version part. The question seems
settled now.

### [Devuan Editors][22]

In a controversial thread, __Community polls on Devuan design__,
jaromil invited community members to setup a poll to measure the
popularity of the [variety of logo proposals][23] and steer towards a
_beta_ design to accompany the __upcoming _alpha_ release__.

Of course most people started pointing at their favorite, but a sneaky
team of pirates including cocoadaemon, golinux, and hellekin, voted
themselves into a design team.

> Appearance is not to be confused with design.

Alban "cocoadaemon" Crommer designed the acclaimed logo that elegantly
mixes in the Debian spiral, and _[Fira Sans][24]_ type commissioned by
Mozilla for Firefox OS.  Unless our readership kills us with
inflammatory comments, it darn looks like the Devuan logo.  Next thing
we know is that they're already planning on delivering some kickass
design across the spectrum, from the Web to the desktop.

The ___[#devuan-www][25]___ channel's topic says:

> We're neither Web masters nor slaves, and neither should you.

The Web team is part of the __Devuan editors__, forming a
transdisciplinary team of people interested in information design,
interface design, code, words, and the arts.  Their mission is to
provide presence and identity

### [LILO][26]

william moss points that LILO does not work for UEFI, while ELILO
does.

Daniel Cegiełka points that LILO also fails to boot on several other
configurations. A discussion raises about if LILO should be maintained
inside Devuan and the pros and cons of Grub2.


## Devuan's Not Gnome

DNG is the discussion list of the Devuan Project.

- [Subscribe to the list][subscribe]
- [Read online archives][archives]

-------------------
Devuan Weekly News is made by people like you: you're [welcome to
contribute][wiki]! Feel free to edit. Also you can join and criticize
us at IRC: #devuan-news at freenode

Thanks for reading us. See you next week.

DWN Team
* Noel, "er Envite" (Editor)
* hellekin (Co-writer, markup master)


[1]: https://lists.dyne.org/lurker/message/20150203.010806.bc1ef94b.en.html "fd leak in evdev patch"
[2]: https://lists.dyne.org/lurker/message/20150203.225844.74b7cbf2.en.html "Distribution independent installer"
[3]: https://lists.dyne.org/lurker/message/20150204.005314.3cf763fa.en.html "Bastille-linux doesn't work with debian anymore"
[4]: https://lists.dyne.org/lurker/message/20150204.005800.bc72a2dc.en.html "Some songs and ideas"
[5]: http://datalove.me/ "Locking data is a crime against datanity"
[6]: https://lists.dyne.org/lurker/message/20150204.144128.1fd21f69.en.html "device management"
[7]: https://lists.dyne.org/lurker/message/20150203.014030.86a446b9.en.html "Towards systemd-free packages"
[8]: https://git.devuan.org/devuan/devuan-project/issues/6 "Packages to be recompiled for standard desktop install"
[9]: https://git.devuan.org/groups/packages-base/issues "packages-base issues"
[10]: https://lists.dyne.org/lurker/thread/20150204.154335.384bb362.en.html "Downgrades may be needed"
[11]: https://lists.dyne.org/lurker/message/20150203.183150.b8ae5e1d.en.html "jenkins"
[12]: https://lists.dyne.org/lurker/message/20150203.190019.136378de.en.html "lobbying upstreams"
[13]: https://lists.dyne.org/lurker/message/20150203.191550.4428d248.en.html "Trinity Desktop Environment"
[14]: https://lists.dyne.org/lurker/message/20150204.003418.8503a935.en.html "systemd dependency through Utopia"
[15]: https://lists.dyne.org/lurker/message/20150203.235805.42259ff1.en.html "Devuan and Trinity DE"
[16]: https://lists.dyne.org/lurker/message/20150204.225056.e18aff11.en.html "Enlightenment"
[17]: https://lists.dyne.org/lurker/message/20150205.034719.90b41571.en.html "XFCE on FreeBSD"
[18]: https://lists.dyne.org/lurker/message/20150205.142408.33c92e82.en.html "Gufw and systemd"
[19]: https://lists.dyne.org/lurker/message/20150206.175046.73deb423.en.html "KDE without systemd"
[20]: https://lists.dyne.org/lurker/message/20150206.155327.1768527f.en.html "CrunchBang Linux"
[21]: https://lists.dyne.org/lurker/message/20150204.112853.d13d82ce.en.html "package versions"
[22]: https://lists.dyne.org/lurker/message/20150208.105542.c51f8c25.en.html "Community polls on Devuan design"
[23]: http://without-systemd.org/wiki/index.php/Category:Logo "Without-Systemd Wiki Category:Logos"
[24]: ircs://irc.freenode.net/devuan-www "IRC channel for the Devuan Web presence"
[25]: https://en.wikipedia.org/wiki/Fira_Sans "Fira Sans"
[26]: https://lists.dyne.org/lurker/message/20150131.013509.90aa459a.en.html "LILO"

[subscribe]: https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng "Subscribe to DNG"
[archives]: https://lists.dyne.org/lurker/list/dng.en.html "Read DNG List Archive"
[current]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/current-issue "Current Issue"
[wiki]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/home "Devuan Weekly News"
