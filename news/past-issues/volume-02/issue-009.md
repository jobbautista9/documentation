# [Devuan Weekly News][current] Issue IX

__Volume 02, Week 4, Devuan Week 9__

https://git.devuan.org/Envite/devuan-weekly-news/past-issues/volume-02/issue-009

## Editorial

This week we have seen a very technical discussion, with so varied and
interesting themes like UEFI and GPT, TPM, PulseAudio, upgrade paths
and versioning. And, of course, the star development of libsysdev.
I'm sure we all are happy that things behave this way.

But as we have noticed before, the list seems separated from the main
development effort, which seems to be hidden in some secluded place.

I'm personally sure this is not on purpose, but from this humble
tribune of Devuan Weekly News, I ask the main developers of Devuan
Jessie (the name is official, now) to take care of being perceived
as more public than they seem now.

Finally, I wish to invite to a discussion about the name of _Devuan
Weekly News_. It has been suggested that it could be improved, and more
clearly distinguished from Debian's DWN.


## Comings and Goings

Daniel Pecka announced his presence to the list.  Please [welcome
Daniel][4]!


## Last Week in Devuan

### [The Countdown][1]

Some impatience is building up.  Eyes are turned towards Franco as the
announced deadline approaches.  Franco Lanza confirmed that the last
big chunk to break down before release is the build server for the ARM
platform.  Detractors and supporters are holding their breath for this
most anticipated alpha release.  KatolaZ reminds the Debian motto that
'releases should happen "when it's time", not accordingly to a fixed
or prescribed schedule.'

### [Upgrade Paths][5]

Our editor Noel Torres is concerned about the upgrade path from
Debian stable and testing to the upcoming Devuan release, that he
nicknamed _Alhambra_ or _Aiken_.  Franco Lanza insists that
part of a smooth upgrade involves keeping the first release named
_Jessie_, and that later releases will have their own names.

Supported upgrade paths include:

- Debian Wheezy (`apt-get dist-upgrade`)
- Debian Jessie (script and fixing dependencies)
- Devuan installer

Gnome users should prepare for a little more work.

### [Audio Configuration][6]

The PulseAudio replacement thread turns into a useful audio
configuration information exchange for pulseaudio and Jack.  Joel Roth
provides extensive [setup information][7].

### [UEFI, GPT][8]

The start of the thread raised some doubts about if Debian (and
consequently Devuan) is able to properly boot from GPT partitioned
disks, which seems to imply using UEFI.

Just to be clear: yes Debian (and Devuan) can work with UEFI and GPT
partition table schemes.  It seems you just need to avoid hardware
with known issues.

Also an interesting reminder from T.J. Duchene:
> If you have Windows or Linux installed in UEFI mode, you MUST
> install the other in the same fashion if you expect Grub to
> dual-boot properly.

### [TPM][10]

The [old TPM thread][11] resurrects with a comment explaining why it
may be desirable to have it on hardware. Or at least desirable for
somebody.

### [Versioning][12]

tilt raises again the question of versioning the packages for Devuan,
discussed mid-December with no clear conclusion.


## systemd News

### [networkd][9]

Martijn Dekkers reports that systemd is now deeping into the network
system. This seems to be done through networkd. Comparison with The
One Ring and black-box transistor chips were unavoidable.


## New Releases

### [libsysdev v0.1.0][2]

Isaac Dunham announced the first version of libsysdev, "a library that
aims to provide an easy-to-use API to get information about devices
from sysfs."  Meanwhile, a lengthy discussion has been going on under
[another thread][3] about coding style, vdev and udev compatibility, etc.

Source code: https://github.com/idunham/libsysdev


## Devuan's Not Gnome

DNG is the discussion list of the Devuan Project.

- [Subscribe to the list][subscribe]
- [Read online archives][archives]

-------------------
We at DWN are humans and may both benefit from collaboration and make
mistakes. Feel free to join and criticize us at IRC: #devuan-news
at freenode

You can collaborate and [edit Devuan Weekly News too][wiki]!

Thanks for reading so far.
Read you next week!

DWN Team
* Noel, "er Envite" (Editor)
* hellekin (Co-writer, markup master)


[1]: https://lists.dyne.org/lurker/thread/20150124.212910.edaa5f2d.en.html "The Countdown"
[2]: https://lists.dyne.org/lurker/message/20150121.211136.e764d681.en.html "Announcing libsysdev 0.1.0"
[3]: https://lists.dyne.org/lurker/message/20150119.190115.cb63c930.en.html "libsysdev preview"
[4]: https://lists.dyne.org/lurker/message/20150121.131831.cca7d312.en.html "Hello PPL, just entered a list .."
[5]: https://lists.dyne.org/lurker/message/20150121.230504.4967da9e.en.html "Upgrade Paths"
[6]: https://lists.dyne.org/lurker/message/20150123.101608.95bf3882.en.html "sugestion apulse as pulse..."
[7]: https://lists.dyne.org/lurker/message/20150125.014214.e0aad07a.en.html "Joel's Jack setup"
[8]: https://lists.dyne.org/lurker/message/20150120.032701.f7f0384e.en.html "UEFI, GPT"
[9]: https://lists.dyne.org/lurker/message/20150115.015653.74673741.en.html "systemd's network"
[10]: https://lists.dyne.org/lurker/message/20150123.185111.7f4fbb33.en.html "TPM"
[11]: https://lists.dyne.org/lurker/message/20141222.214404.dd78e3ac.en.html "Old TPM thread"
[12]: https://lists.dyne.org/lurker/message/20150125.000430.437adbd6.en.html "Versioning"


[subscribe]: https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng "Subscribe to DNG"
[archives]: https://lists.dyne.org/lurker/list/dng.en.html "Read DNG List Archive"
[current]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/current-issue "Current Issue"
[wiki]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/home "Devuan Weekly News"
