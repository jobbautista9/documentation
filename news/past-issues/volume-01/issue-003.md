Author: Noel Torres

Date: 2014-12-17 06:07 -000

To: dng

Subject: [Dng] Devuan Weekly News III


Welcome to Devuan Weekly News III. I know you all were awaiting this for
yesterday, but Real Life (TM) has some issues that can not be resolved by
simply applying a patch and restarting a service.

Hands have appeared to help, but the actual jobs assignment is still pending.
Hands are still welcomed, anyway.

This week we see more settlement on the list, with better quality comments in
general, but still not so much technical work. I am very happy to say in this
editorial comment that there has been almost no harsh this week on the list.
Good work everybody!

As an internal issue, there has been some work on Devuan Weekly News name. A
proposal is renaming as Devuan Vuakly News. Another is to keep actual name,
but to use DvWN as acronym, and DbWN when it comes to us talking about Debian
Weekly News.

Issue II ([1]) somehow mutated into a discussion about the right of bears to
bear arms ([2]) by means of a post at [3] that should have been better
addressed at the Devuan Constitution thread ([4]). This thread, on its own,
has continued discussion about non-free ([5]), the real usability of a system
without it ([6]), which derived in some apt/dpkg praise at [7], and the
convenience for Devuan not to drop non-free in order to be relevant ([8]). A
related thread ([9]) discusses the packaging system.

[1] https://lists.dyne.org/lurker/message/20141209.035425.f77e1735.en.html

[2] https://lists.dyne.org/lurker/message/20141209.055352.04a3b83b.en.html

[3] https://lists.dyne.org/lurker/message/20141209.041940.41691d9a.en.html

[4] https://lists.dyne.org/lurker/message/20141205.213027.15680885.en.html

[5] https://lists.dyne.org/lurker/message/20141212.012326.45c306cb.en.html

[6] https://lists.dyne.org/lurker/message/20141212.030951.31bc38a0.en.html

[7] https://lists.dyne.org/lurker/message/20141213.185736.5b32e478.en.html

[8] https://lists.dyne.org/lurker/message/20141206.205110.80b53316.en.html

[9] https://lists.dyne.org/lurker/message/20141215.042743.f8d56edf.en.html

The discussion about release names continued with a thread specifically devoted
to the Frank Zappa songs option at [10].

[10] https://lists.dyne.org/lurker/message/20141208.060640.6b9a2355.en.html

The thread about GTK ([11]) is still on, with some seemingly consensus about
GTK being a bad toolkit ([12]), and an extra thread at [13] including some
interesting links ([14]).

[11] https://lists.dyne.org/lurker/message/20141206.135753.2d326577.en.html

[12] https://lists.dyne.org/lurker/message/20141206.215142.302aa36d.en.html

[13] https://lists.dyne.org/lurker/message/20141209.100152.53876559.en.html

[14] https://lists.dyne.org/lurker/message/20141210.171807.976da7ac.en.html

There was a thread about an actual virtual machine without systemd ([15]) and
whether if XFCE depends on it ([16]), with the conclusion that it is possible
to use Debian's Jessie with no issues without systemd, but with libsystemd
"lying around" ([17]).

[15] https://lists.dyne.org/lurker/message/20141209.153229.8a3ae330.en.html

[16] https://lists.dyne.org/lurker/message/20141210.003828.fb4c8a4e.en.html

[17] https://lists.dyne.org/lurker/message/20141210.224020.02a3da35.en.html

A thread at [18] presented a new distro named TRIOS based on Debian and
without systemd.

[18] https://lists.dyne.org/lurker/message/20141210.175347.503b0740.en.html

A less technical thread started at [19] about some declarations by Mr.
Poettering about Devuan. As it could have been easily imagined there were
requests to just ignore him, which raised a post about the real fails of
systemd ([20]), very centered. There were also comments about Mr. Poettering's
words being free publicity for the project ([21]) and some interesting history
lessons at [22]. As an editorial note from Devuan Weekly News, it is a matter
of respect not to insult anybody nor to mock on anybody's name. We should
respect, even to those who does not respect Devuan or our views, or even us.

[19] https://lists.dyne.org/lurker/message/20141209.131002.109d4e4a.en.html

[20] https://lists.dyne.org/lurker/message/20141209.194313.b96ad4d1.en.html

[21] https://lists.dyne.org/lurker/message/20141211.012635.b51c35d7.en.html

[22] https://lists.dyne.org/lurker/message/20141211.180145.92547e37.en.html

The logo issue continues at its own pace ([23], [24], [25], [26]) with some
interesting approachs like using ࿕ (U+0FD5) as it has world-wide positive
connotations and a single, localized negative one ([27]). Same slow pace on
the original thread about release names ([28]).

[23] https://lists.dyne.org/lurker/message/20141212.174743.75ddd9b1.en.html

[24] https://lists.dyne.org/lurker/message/20141213.135114.8f28333c.en.html

[25] https://lists.dyne.org/lurker/message/20141214.124715.6c6df3ad.en.html

[26] https://lists.dyne.org/lurker/message/20141215.040525.30f7b76d.en.html

[27] https://lists.dyne.org/lurker/message/20141213.033358.280298dd.en.html

[28] https://lists.dyne.org/lurker/message/20141207.125705.b8eb0f0a.en.html

Another big thread was the one at [29] about women on Devuan. It seems that
the number is quite low, which has caused ([30]) some fun, but also
interesting reflexions around "good code is good code, no matter who".

[29] https://lists.dyne.org/lurker/message/20141211.082341.9cb9838c.en.html

[30] https://lists.dyne.org/lurker/message/20141211.102016.af63c656.en.html

There was a question about donations at [31] with an answer afterwards.

[31] https://lists.dyne.org/lurker/message/20141211.085556.93430384.en.html

The thread about GNOME and GDM [32] is still alive.

[32] https://lists.dyne.org/lurker/message/20141205.125409.30d7a092.en.html

Also, it has been proposed ([33]) to name our first release, matching Debian's
Jessie, just as Jessie as well. The proposal seems to have traction. Also it
is under discussion ([34]) how (if) we should add a suffix to the packages we
modify.

[33] https://lists.dyne.org/lurker/message/20141215.160426.238a14ca.en.html

[34] https://lists.dyne.org/lurker/message/20141215.180911.7a38dbef.en.html

It is still active as well the thread ([35]) about the scripting languages,
now discussing about udev ([36]).

[35] https://lists.dyne.org/lurker/message/20141206.030135.8c980889.en.html

[36] https://lists.dyne.org/lurker/message/20141216.203705.4f3eaf47.en.html

Last, but not least, a thread at [37] is very interesting from start in that
it clearly expresses that majority is not the way to go when one needs to get
the correct answer to some problem. I recommend to read the complete thread,
think twice, and then provide a chilled opinion.

[37] https://lists.dyne.org/lurker/message/20141215.004830.628242ed.en.html

Well, this time I have had no sleepless monday night. It has been tuesday
night instead.

Look for Issue IV!

Thanks for reading so far

Noel

er Envite

monoprocessor human being compatibilizing Family and DWN 