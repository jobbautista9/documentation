# Devuan Project Documentation

## Devuan specific documentation

 * [devuan art](./art/)
 * [devuan project docs](./project/)
 * [devuan maintainers docs](./maintainers/)
 * [devuan news docs](./news/)
 * [dev1fanboy's wiki and pages](./dev1fanboy/)
 * [Manuals](./manuals/)


## Contributing

In order to contribute to this documentation, simply fork the project and create a Merge Request.
After having seen your contributions for a while, you may request developer access, which will allow you to contribute on this repository directly.
