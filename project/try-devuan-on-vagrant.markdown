# Devuan on Vagrant

A [developer release for i386](https://atlas.hashicorp.com/jaromil/boxes/devuan-alpha-i386) is available on [Vagrant](https://vagrantup.com/).

## Install Vagrant

Your OS should provide a package for `vagrant`.  If it does not, you can [install it via Rubygems](https://docs.vagrantup.com/v2/installation/index.html).

The developer release requires `VirtualBox` to run.

## Get Started

```
mkdir ~/vagrant && cd $_
vagrant init jaromil/devuan-alpha-i386
vagrant up --provider=VirtualBox
```

Once the machine is running, you can `ssh` into it (user and password are `devuan`):
```
ssh -p 2222 devuan@localhost
```
