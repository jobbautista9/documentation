----------------------

Dmenu howto for Devuan (INCOMPLETE5)

----------------------

# What is dmenu?

Dmenu is a keyboard driven GUI command runner that lists every executable
command on the path, accepts user keystrokes, and limits the executable list
to commands containing the string typed so far. To run a command on the list,
keep typing keys to reduce the list, until either there's one command left, in
which case you press Enter to run the command, or until it's practical to
cursor to the desired command and press Enter to run the command.  A command
can typically be executed with four to six keystrokes.

In other words, if you associate a quick and easy hotkey to dmenu, dmenu is an
extremely efficient way to run executables whose name you know. To run
`iceweasel`, you type the hotkey and then `icew`, and Iceweasel runs. Most
touch typists can't reach for their mouse that fast.

Speaking of fast, dmenu is so simple and efficient that there's no
human-perceptable delay on running dmenu, or after each keystroke. Running the
executable takes the same time it would take from a command prompt.  Dmenu is
lightning fast in its runtime performance, and adapted to the touch typist to
maximize productivity.

## I like my system menu

That's fine. Using dmenu doesn't preclude using your system menu, and using
your system menu doesn't preclude using dmenu. Your system menu is built for
discoverability, and dmenu is built for speed. You're free to use both.

## I've heard dmenu is ugly and hard to read

As it comes out of the box, that's usually true, especially for people with less than 
stellar visual acuity. But consider the following:

* You can make the executable list go down the screen instead of across the top.
* You can change the font size of the list.
* You can change the foreground and background colors of the list, both in normal and highlighted mode.

## Dmenu has very few dependencies

Dmenu isn't, and almost certainly never will be, dependent on systemd or any
systemd dependent software.  The dmenu README file lists the Xlib headers as
its only dependency. Its config.mk file also lists Xinerama as a dependency,
but that can be commented out if it's a problem.

# How to Install dmenu

There are two ways to install dmenu:

1. With the Devuan dmenu package
0. Compiling code from http://tools.suckless.org/dmenu/

Your installation method also affects your configuration of dmenu, because
package file locations are different from those of the self-compiled version,
and the method of getting the executable path differs between the two, also.
This document tells you both methods of installation, with their respective
configurations.

# Installing dmenu From Your Devuan Package Manager

This will be written later, after the Devuan's dmenu package is in its final
form.

# Installing dmenu From Source Code

Before installing it, make sure you don't already have dmenu installed. Run the following two commands:

* `dmenu`
* `dmenu_run`

Both commands should return "command not found" errors if it's not already
installed. If they don't return "command not found" errors, you already have
dmenu installed, so you should either work with the installed dmenu software,
or uninstall it and then install from source code.

Here's how you do it:

1. Download the tarball from http://tools.suckless.org/dmenu/. Current tarball is version 4.5.
0. mkdir ~/dmbld; cd ~/dmbld 
0. Copy the tarball from step 1 into ~/dmbld
0. In the following step, use the filename of the actual tarball you downloaded.
0. `tar xzvf dmenu-4.5.tar.gz`
0. With your package manager, make sure you install the Xlib headers. Also install Xinerama.
0. If you *cannot* install Xinerama, comment out the `XINERAMALIBS` and `XINERAMAFLAGS` definition lines in `config.mk`
0. In the following step, adjust the directory for your dmenu version. Current version as of 3/2015 is 4.5.
0. `cd dmenu-4.5`
0. `make clean install`
0. Type `dmenu_run` at the command prompt, and verify that a list of executables, probably in very tiny fonts, is printed across the very top of your screen. Be very observant: The list might be in a tiny font with little contrast. If the list is visible at the very top of your screen, dmenu is installed. If not, troubleshoot.
0. To exit dmenu and clear the list from the top of the screen, press the Esc key.

## Running dmenu *your* way

When you ran `dmenu_run`, did you notice how tiny the characters were? Did you
notice the lack of contrast?  To make the menu look how you want, make a
script called dmenu_myway, and put it on the executable path. To start, create
dmenu_myway as follows:

	#!/bin/sh
	dmenu_run -fn 10x20 -nf white -nb black -sf blue -sb yellow -l 25

The following info describes the preceding `dmenu_myway` shellscript:

* `-fn 10x20` is the font size. Typical font sizes are 6x12 and 7x14 for perfect vision types, 8x16 (ugly), 
9x15, 10x20, and 10x24 (ugly, but big and readable).
* `-nf white` is the non-highlighted foreground color. Colors can also be specified as `"#rrggbb"`,
where rr, gg and bb are hex numbers between 00 and ff, and the doublequotes are mandatory.
* `-nb black` is the non-highlighted background color.
* `-sf blue` is the foreground color of the highlighted item.
* `-sb yellow` is the background color of the highlighted item.
* `-l 25` states that items run down the screen instead of across the top, and that there will be 25
lines of such items. If you prefer your choices to run across the top of the screen,
eliminate both the `-l` and number that follows it.
* If you `-l` without a number, it fails. 
* All values *must* be separated from their option by a space. For instance, `-l25` fails, you need to write `-l 25`
* There are other, less important adjustments you can make. See the dmenu man page for details.
* Adjust all these parameters until dmenu delivers the fastest recognition for you. After all,
speed is the reason for dmenu's existance.
* Don't worry how much of the screen dmenu covers. While running, dmenu takes over focus and your keyboard,
and the minute you've either Escaped out or chosen a program, it vanishes.

# Invoking dmenu with a hotkey

Obviously, dmenu would be the slowest possible interface if, every time you
wanted to select a program, you had to find a terminal and type `dmenu_myway`.
You need a hotkey. Different people have different hotkey preferences. There's
no "one size fits all" hotkey recommendation. Here are your considerations
when choosing a hotkey:

* Must be easy to type and lightning fast.
* For touch typists, something available from home position is often the best.
* Must be memorable to you.
* Must not conflict with OS, Desktop Environment or application hotkeys.
* Must not cause repetitive motion injuries.

Setting a hotkey for an application (such as dmenu) is performed at the
Desktop (DE) Environment level, with each DE having its own way to set
hotkeys.

One more consideration is that name representations of keyboard keys vary from
DE to DE, and capitalization counts. Your best way to view all possible key
representations is to run the `xev` program from a terminal. Type various
keys, and watch their representations show up on the terminal. For instance,
the following is what happened when I pressed and released the Semicolon (;)
key:

	KeyPress event, serial 45, synthetic NO, window 0x6600001,
	  root 0x43, subw 0x0, time 1342397047, (372,303), root:(1862,793),
	  state 0x0, keycode 47 (keysym 0x3b, semicolon), same_screen YES,
	  XLookupString gives 1 bytes: (3b) ";"
	  XmbLookupString gives 1 bytes: (3b) ";"
	  XFilterEvent returns: False
	
	KeyRelease event, serial 45, synthetic NO, window 0x6600001,
	  root 0x43, subw 0x0, time 1342397126, (372,303), root:(1862,793),
	  state 0x0, keycode 47 (keysym 0x3b, semicolon), same_screen YES,
	  XLookupString gives 1 bytes: (3b) ";"
	  XFilterEvent returns: False

As you can see from above, the Semicolon could be represented by the string
"semicolon" (note all lowercase), the hexidecimal key symbol 0x3b, or ";".
Different representations are used by different DEs, and each is used by at 
least one DE.

As of 3/1/2015, it looks like Devuan's default DE will be Xfce, so this
section discusses how to set an Xfce based hotkey to run `dmenu_myway`.

